package com.ashokit.data.jpa.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "EMP")

@NamedQueries({
	@NamedQuery(name = "EmployeeEntity.readEmpsOfDept", query = "select e from EmployeeEntity e where e.deptno = ?1")
})


public class EmployeeEntity {
	@Id
	@Column(name = "EMPNO")
	private Integer empId;
	
	@Column(name = "NAME")
	private String empName;
	
	@Column(name = "SAL")
	private Double empSal;
	
	private Integer deptno;

	
	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public Double getEmpSal() {
		return empSal;
	}

	public void setEmpSal(Double empSal) {
		this.empSal = empSal;
	}

	public Integer getDeptNo() {
		return deptno;
	}

	public void setDeptNo(Integer deptno) {
		this.deptno = deptno;
	}

	public EmployeeEntity(Integer empId, String empName, Double empSal, Integer deptno) {
		super();
		this.empId = empId;
		this.empName = empName;
		this.empSal = empSal;
		this.deptno = deptno;
	}

	public EmployeeEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "EmployeeEntity [empId=" + empId + ", empName=" + empName + ", empSal=" + empSal + ", deptno=" + deptno
				+ "]";
	}
	
	

}
