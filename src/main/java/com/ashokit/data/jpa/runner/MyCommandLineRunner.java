package com.ashokit.data.jpa.runner;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.ashokit.data.jpa.dao.EmployeeEntityRepository;
import com.ashokit.data.jpa.entity.EmployeeEntity;

@Component
public class MyCommandLineRunner implements CommandLineRunner {

	@Autowired
	EmployeeEntityRepository repo;

	@Override
	public void run(String... args) throws Exception {
		// ************** save employee **********************

		/*
		 * EmployeeEntity entity = new EmployeeEntity(); entity.setEmpId(115);
		 * entity.setEmpName("Mona Pawar"); entity.setEmpSal(5000.0);
		 * entity.setDeptNo(10);
		 * 
		 * repo.save(entity);
		 */

		// ************ find by id **********************
/*
		Optional<EmployeeEntity> opt = repo.findById(110);
		if (opt.isPresent()) {
			EmployeeEntity e = opt.get();
			System.out.println(e.getEmpId() + ", " + e.getEmpName() + ", " + e.getEmpSal() + ", " + e.getDeptNo());
		}
*/
		// ************ findAll ************************

		/*
		List<EmployeeEntity> lstEmp = repo.findAll();
		lstEmp.forEach(System.out::println);
		System.out.println("==============================================================");
		// ************ findByEmpSalGreaterThanEqual ****************
		List<EmployeeEntity> newLstEmp = repo.findByEmpSalGreaterThanEqual(7000.0);
		newLstEmp.forEach(System.out::println);
		System.out.println("==============================================================");

		List<String> lstNames = repo.fetchEmpsNamesOfDeptBySal(10, 15000.0);
		lstNames.forEach(System.out::println);

		List<EmployeeEntity> lst = repo.fetchHighestPaidEmployee();
		lst.forEach(System.out::println);
		System.out.println("==================================================");
		Integer i = repo.deleteEmpsOfDept(40);
		System.out.println(i + " row(s) deleted.");
		System.out.println("===================================================");
		List<EmployeeEntity> lst2 = repo.readEmpsOfDept(20);
		lst2.forEach(System.out::println);
		*/
		
		List<EmployeeEntity> list1=repo.findByEmpSalBetween(5000.0, 30000.0);
		list1.forEach(System.out::println);
		
		List<EmployeeEntity> list2=repo.findByEmpName("Mona Pawar");
		list2.forEach(System.out::println);
		
		

	}

}
